package com.example.davidgeisinger.faultinourstreets;

import android.content.Intent;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import java.nio.charset.StandardCharsets;

/**
 * Created by davidgeisinger on 10/13/15.
 */
public class PhoneListenerService extends WearableListenerService {
    private static final String START_PICS = "/start_pics";
    private static final String back_to_list = "/list";

    private static final String TAG = "MESSAGEBACKTOPHONE";


    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if( messageEvent.getPath().equalsIgnoreCase(START_PICS)) {
            String value = new String(messageEvent.getData(), StandardCharsets.UTF_8);


            Intent serviceIntent = new Intent(this, InstagramActivity.class);
            serviceIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            serviceIntent.putExtra("to_act", value);
            serviceIntent.setAction("SHAKEYSHAKE");
            startActivity(serviceIntent);


        } else if( messageEvent.getPath().equalsIgnoreCase(back_to_list)) {
            Intent serviceIntent = new Intent(this, DummyActivity.class);
            serviceIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(serviceIntent);


        } else {
            super.onMessageReceived(messageEvent);
        }

    }
}
