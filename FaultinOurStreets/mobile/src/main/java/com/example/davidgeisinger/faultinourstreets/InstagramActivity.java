package com.example.davidgeisinger.faultinourstreets;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class InstagramActivity extends AppCompatActivity {

    String grab_data;
    String lat;
    String lon;
    ArrayList for_pics = new ArrayList<String>();
    ArrayList for_text = new ArrayList<String>();

    String TTW = "/text_to_watch";
    private GoogleApiClient mApiClient;
    ImageView image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instagram);
        image = (ImageView) findViewById(R.id.iv);

        mApiClient = new GoogleApiClient.Builder( this )
                .addApi( Wearable.API )
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        /* Successfully connected */
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                        /* Connection was interrupted */
                    }
                })
                .build();

        Bundle mybundle = getIntent().getExtras();
        String [] arr = mybundle.getString("to_act").split("!");
        lon = arr[0];
        lat = arr[1];

        start_moving();

    }


    public void start_moving() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                URL url;
                String urlstr;
                try {
                    urlstr = "https://api.instagram.com/v1/media/search?lat=" + lat + "&lng=" + lon + "&client_id=" + "9336576f23a9447989a65debfe1d587b" + "&distance=4000";
                    url = new URL(urlstr);
                } catch (MalformedURLException e) {

                    e.printStackTrace();
                    return;
                }

                HttpURLConnection urlConnection = null;
                try {
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.connect();
                    InputStream in = urlConnection.getInputStream();
                    Scanner scanner = new Scanner(in);
                    //Scanners scan line by line: if your response is longer than 1 line, you need a loop
                    while (scanner.hasNextLine()) {
                        grab_data += scanner.nextLine(); //parses the GET request into a string
                    }
                    parse_data(grab_data);
                    parse_text(grab_data);
                    String final_url;
                    String final_text;
                    if (for_pics.size() == 0) {
                        final_url = "http://www.keralabjp.org/images/No_Image.jpg";
                        final_text = "No posts, Sorry!";
                    } else {
                        final_url = choose_photo(for_pics);
                        final_text = choose_photo(for_text);
                    }
                    sendMessage(TTW, final_text);
                    final Bitmap b = BitmapFactory.decodeStream((InputStream)new URL(final_url).getContent());
                    InstagramActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            image.setImageBitmap(b);
                            }
                    });

                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }
            }
        }).start();

    }

    public void parse_data(String grabbed) {
        String regex = "standard_resolution";
        ArrayList<String> pics = new ArrayList<String>();
        String [] arr = grabbed.split(regex);
        for (int i = 1; i < arr.length; i++) {
            arr[i] = arr[i].substring(10);
            String [] temp = arr[i].split("\\\"");
            arr[i] = temp[0];
            arr[i] = arr[i].replace("\\", "");
           for_pics.add(arr[i]);
        }

    }

    public void parse_text(String grabbed) {
        String regex = "text";
        ArrayList<String> pics = new ArrayList<String>();
        String [] arr = grabbed.split(regex);
        for (int i = 1; i < arr.length; i++) {
            arr[i] = arr[i].substring(3);
            String [] temp = arr[i].split("\\\"");
            arr[i] = temp[0];
            arr[i] = arr[i].replace("\\", "");
            for_text.add(arr[i]);
        }

    }


    //How to send a message to the WatchListenerService
    public void sendMessage( final String path, final String text) {

        new Thread( new Runnable() {
            @Override
            public void run() {
                mApiClient.connect();
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( mApiClient ).await();
                for(Node node : nodes.getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mApiClient, node.getId(), path, text.getBytes() ).await();
                }
            }
        }).start();
    }

    public String choose_photo(ArrayList<String> pics) {
        Random rando = new Random();
        int index = rando.nextInt(pics.size() + 1);

        String my_pic = pics.get(index);
        return my_pic;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mApiClient.disconnect();
    }

}
