package com.example.davidgeisinger.faultinourstreets;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.Wearable;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private static final String START = "/start_activity";
    private final static String TAG = "MainActivity11";
    ListView lv;
    ArrayAdapter<EarthquakeInfo> arrayAdapter;
    List<EarthquakeInfo> your_array_list;
    EarthquakeInfo curr_earth;
    private GoogleApiClient mGoogleApiClient;
    double myLatitude = 1;
    double myLongitude = 4.2;
    TextView tv;

    private GoogleApiClient mApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        /* Successfully connected */
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                        /* Connection was interrupted */
                    }
                })
                .build();

        // Get my location really quick
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        // done getiing location


        registerReceiver(myReceiver, new IntentFilter("Test message"));


        //list view setup

        lv = (ListView) findViewById(R.id.earthquakelv);



            your_array_list = new ArrayList<EarthquakeInfo>();
            arrayAdapter = new ArrayAdapter<EarthquakeInfo>(
                    this,
                    android.R.layout.simple_list_item_1,
                    your_array_list);


            lv.setAdapter(arrayAdapter);

        // all list view setup

        // setContentView(R.layout.activity_main);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Object item = parent.getItemAtPosition(position);
                EarthquakeInfo real_item = (EarthquakeInfo) item;
                String to_send = real_item.magnitude + "!" + real_item.city_origin + "!" + real_item.longitude + "!" + real_item.latitude;

                Intent serviceIntent = new Intent("xxx");
                serviceIntent.putExtra("to_efs", to_send);
                sendBroadcast(serviceIntent);

                Intent intent = new Intent(MainActivity.this, MapsActivity.class);

                String x = real_item.latitude;
                String y = real_item.longitude;
                intent.putExtra("x", x);
                intent.putExtra("y", y);
                //based on item add info to intent
                startActivity(intent);

            }


        });
    }






    private BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            // Get extra data included in the Intent
            String message = intent.getStringExtra("message");
            EarthquakeInfo quaker = new EarthquakeInfo(message, myLatitude, myLongitude);
            curr_earth = quaker;
            arrayAdapter.insert(quaker, 0);

            tv = (TextView) findViewById(R.id.tv1);
            tv.setText(quaker.toString1());
            tv.setBackgroundColor(Color.BLACK);


            Intent newintent = new Intent(MainActivity.this, MapsActivity.class);
            String x = quaker.latitude;
            String y = quaker.longitude;
            newintent.putExtra("x", x);
            newintent.putExtra("y", y);
            //based on item add info to intent
            startActivity(newintent);


        }
    };





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGoogleApiClient.disconnect();
    }


    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connResult) {
    }


    @Override
    public void onConnected(Bundle bundle) {
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        String myLat = "aa";
        String myLon = "a";
        if (mLastLocation != null)

        {
            myLat = String.valueOf(mLastLocation.getLatitude());
            myLon = String.valueOf(mLastLocation.getLongitude());
            myLatitude = mLastLocation.getLatitude();
            myLongitude = mLastLocation.getLongitude();
        }
        Intent serviceIntent = new Intent(this, EarthquakeFinderService.class);
        serviceIntent.putExtra("mylat", String.valueOf(myLatitude));
        serviceIntent.putExtra("mylon", String.valueOf(myLongitude));
        startService(serviceIntent);

    }


    @Override
    public void onLocationChanged(Location location) {

    }


}
