package com.example.davidgeisinger.faultinourstreets;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.Wearable;



    public class GPSActivity extends Activity implements
            GoogleApiClient.ConnectionCallbacks,
            GoogleApiClient.OnConnectionFailedListener, LocationListener {

        private GoogleApiClient mGoogleApiClient;
        public static String TAG = "GPSActivity";
        public static int UPDATE_INTERVAL_MS = 1000;
        public static int FASTEST_INTERVAL_MS = 1000;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addApi(Wearable.API)  // used for data layer LAPI
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();

        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);

        if(mLastLocation!=null)

        {
            String myLat = String.valueOf(mLastLocation.getLatitude());
            String myLon = String.valueOf(mLastLocation.getLongitude());
        }

    }

        @Override
        protected void onResume() {
            super.onResume();
            mGoogleApiClient.connect();
        }

        @Override
        protected void onPause() {
            super.onPause();
            mGoogleApiClient.disconnect();
        }


        @Override
        public void onConnectionSuspended(int i) {
        }

        @Override
        public void onConnectionFailed(ConnectionResult connResult) {
        }



            @Override
            public void onConnected(Bundle bundle) {

                // Build a request for continual location updates
                LocationRequest locationRequest = LocationRequest.create()
                        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                        .setInterval(UPDATE_INTERVAL_MS)
                        .setFastestInterval(FASTEST_INTERVAL_MS);

                // Send request for location updates
                LocationServices.FusedLocationApi
                        .requestLocationUpdates(mGoogleApiClient,
                                locationRequest, this)
                        .setResultCallback(new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                if (status.getStatus().isSuccess()) {
                                    Log.d(TAG, "Successfully requested");
                                } else {
                                    Log.e(TAG, status.getStatusMessage());
                                }
                            }
                        });

            }

        @Override
        public void onLocationChanged(Location location) {

        }


           /* @Override
            public void onLocationChanged(Location location){
            // Do some work here with the location you have received
        }*/


        }

