package com.example.davidgeisinger.faultinourstreets;

/**
 * Created by davidgeisinger on 10/11/15.
 */
public class EarthquakeInfo {
    
    String magnitude;
    String city_origin;
    String latitude;
    String longitude;
    String total_dist;
    
    public EarthquakeInfo(String msg_rcvd, double mylat, double mylon) {
        String [] str_arr = msg_rcvd.split("!");
        magnitude = str_arr[0];
        city_origin = str_arr[1];
        longitude = str_arr[2];
        latitude = str_arr[3];
        double ans = distance(Double.parseDouble(latitude), Double.parseDouble(longitude), mylat, mylon);
        //NumberFormat formatter = new DecimalFormat("#0.00");
        //total_dist = formatter.format(Double.toString(ans));
        String total_dist_long = Double.toString(ans);

        total_dist = total_dist_long.substring(0,7);





    }


    private double distance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
            dist = dist * 1.609344;
        return (dist);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts decimal degrees to radians             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts radians to decimal degrees             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public String toString() {
        //return "There was an earthquake of " + magnitude + " in " + city_origin + " at coordinates " + latitude + "," + longitude;

        return "Earthquake in: " + city_origin + "                                   Magnitude: " + magnitude + "                                               " + total_dist + " kms away";
    }

    public String toString1() {
        //return "There was an earthquake of " + magnitude + " in " + city_origin + " at coordinates " + latitude + "," + longitude;

        return "Earthquake in: " + city_origin + "                       Mag: " + magnitude + "        " + total_dist + " kms away";    }
}
