package com.example.davidgeisinger.faultinourstreets;

/**
 * Created by davidgeisinger on 10/10/15.
 */

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;


public class EarthquakeFinderService extends Service {
    private static final String TAG = "EFS";
    private static final int INTERVAL = 3000;
    private static final int SECOND = 1000;
    int count = 0;
    private GoogleApiClient mApiClient;


    /* TIME API DETAILS */
    private String mUrlString = "http://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&limit=1";
    private String mCityResponse = "";
    private String mMagnitudeResponse = "";
    private String curr_city = "";
    private String curr_magnitude = "0.0";
    String mylat= "0";
    String mylon = "0";
    private static final String START_ACTIVITY = "/start_activity";



    @Override
    public void onCreate() {
        super.onCreate();

        /* Initialize the googleAPIClient for message passing */
        mApiClient = new GoogleApiClient.Builder( this )
                .addApi( Wearable.API )
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        /* Successfully connected */
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                        /* Connection was interrupted */
                    }
                })
                .build();

        registerReceiver(myReceiver, new IntentFilter("xxx"));

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Kick off new work to do
        mylat = intent.getStringExtra("mylat");
        mylon = intent.getStringExtra("mylon");
        createAndStartTimer();
        return START_STICKY;
    }


    private void createAndStartTimer() {
        CountDownTimer timer = new CountDownTimer(INTERVAL, SECOND) {
            public void onTick(long millisUntilFinished) { }
            public void onFinish() {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String grab_data = "";
                        String city_origin = "";
                        String magnitude = "";
                        String send_to_watch = "";
                        String concat = "";
                        URL url;
                       /* if (count == 0) {
                            sendMessage(START_ACTIVITY, "4.19!Berkdaddy!6.89!6.89", "100");
                            baby_broadcast("4.19!Berkdaddy!6.89!6.89");
                            count += 1;
                        }*/

                        try {
                            url = new URL(mUrlString);
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                            return;
                        }

                        HttpURLConnection urlConnection = null;
                        try {
                            urlConnection = (HttpURLConnection) url.openConnection();
                            urlConnection.connect();
                            InputStream in = urlConnection.getInputStream();
                            Scanner scanner = new Scanner(in);
                            //Scanners scan line by line: if your response is longer than 1 line, you need a loop
                            while (scanner.hasNextLine()) {
                                grab_data += scanner.nextLine(); //parses the GET request into a string
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            if (urlConnection != null) {
                                urlConnection.disconnect();
                            }
                        }
                        if (!grab_data.equals("")) {
                            String [] arr = grab_data.split("mag");
                            String temp = arr[1];
                            String [] arr1 = temp.split(",");
                            magnitude = arr1[0].substring(2);
                            String [] city_arr = grab_data.split("place");
                            temp = city_arr[1];
                            String [] start_city = temp.split("of");
                            String temp_city = start_city[1];
                            String [] origin_arr = temp_city.split("\",");
                            city_origin = origin_arr[0].substring(1);
                            String [] coords = grab_data.split("coordinates");
                            String almost_coordinates = coords[1].substring(3);
                            String [] final_arr = almost_coordinates.split(",");
                            String latitude = final_arr[0];
                            String longitude = final_arr[1];


                            send_to_watch = magnitude + "!" + city_origin + "!" + latitude + "!" + longitude;
                            EarthquakeInfo quaker = new EarthquakeInfo(send_to_watch, Double.parseDouble(mylat), Double.parseDouble(mylon));

                            String title = quaker.city_origin;
                            String second_line = quaker.magnitude;
                            String distance = quaker.total_dist;



                            if (!curr_city.equals(city_origin) || !magnitude.equals(curr_magnitude)) {
                                curr_city = city_origin;
                                curr_magnitude = magnitude;
                                mApiClient.connect(); //connect to the API client to send a message!
                                sendMessage(START_ACTIVITY, send_to_watch, distance); //actually send the message to the watch
                                //pass extra information of the hour (as a string)
                                baby_broadcast(send_to_watch);
                            }
                        }
                    }
                }).start();

                createAndStartTimer();
            }
        };

        timer.start();

    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    //How to send a message to the WatchListenerService
    public void sendMessage( final String path, final String text, String distance ) {

        final String send_text = text + "!" + distance;
        new Thread( new Runnable() {
            @Override
            public void run() {
                NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes( mApiClient ).await();
                for(Node node : nodes.getNodes()) {
                    MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(
                            mApiClient, node.getId(), path, send_text.getBytes() ).await();
                }
            }
        }).start();
    }

    public void baby_broadcast(final String text) {
        Intent serviceIntent = new Intent("Test message");
        serviceIntent.putExtra("message", text);
        sendBroadcast(serviceIntent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mApiClient.disconnect();
    }

    private BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            // Get extra data included in the Intent
            String message = intent.getStringExtra("to_efs");
            EarthquakeInfo quaker = new EarthquakeInfo(message, Double.parseDouble(mylat), Double.parseDouble(mylon));
            String distance = quaker.total_dist;
            sendMessage(START_ACTIVITY, message, distance);



        }
    };

}
