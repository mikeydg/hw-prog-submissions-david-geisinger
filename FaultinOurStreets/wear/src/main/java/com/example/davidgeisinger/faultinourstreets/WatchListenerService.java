package com.example.davidgeisinger.faultinourstreets;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import java.nio.charset.StandardCharsets;


public class WatchListenerService extends WearableListenerService {
    private static final String START_ACTIVITY = "/start_activity";
    String TTW = "/text_to_watch";

    private static final String TAG = "WLS";


    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if (messageEvent.getPath().equalsIgnoreCase(START_ACTIVITY)) {
            String value = new String(messageEvent.getData(), StandardCharsets.UTF_8);
            String[] arr = value.split("!");
            String title = arr[1];
            String line1 = arr[0];
            String line2 = arr[4];
            String lat = arr[2];
            String lon = arr[3];


            int notificationId = 001;
            Intent viewIntent = new Intent(this, MyDisplayActivity.class);
            viewIntent.putExtra("lat/lon", lat + "!" + lon);
            viewIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent viewPendingIntent =
                    PendingIntent.getActivity(getApplicationContext(), 0, viewIntent, PendingIntent.FLAG_UPDATE_CURRENT);


            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.common_signin_btn_icon_normal_dark)
                            .setContentTitle(title)
                            .setContentText("Dist: " + line2 + " kms" + "         " + "Mag: " + line1)
                            .setDefaults(Notification.DEFAULT_ALL)
                            .setContentIntent(viewPendingIntent);

// Get an instance of the NotificationManager service
            NotificationManagerCompat notificationManager =
                    NotificationManagerCompat.from(this);

// Build the notification and issues it with notification manager.
            notificationManager.notify(notificationId, notificationBuilder.build());





        } else if (messageEvent.getPath().equalsIgnoreCase(TTW)) {

            //Put text into the watch main activity
            String value = new String(messageEvent.getData(), StandardCharsets.UTF_8);
            baby_broadcast(value);

        } else {
            super.onMessageReceived(messageEvent);
        }

    }

    public void baby_broadcast(final String text) {
        Intent serviceIntent = new Intent("xx");
        serviceIntent.putExtra("backatyou", text);
        sendBroadcast(serviceIntent);
    }

}
