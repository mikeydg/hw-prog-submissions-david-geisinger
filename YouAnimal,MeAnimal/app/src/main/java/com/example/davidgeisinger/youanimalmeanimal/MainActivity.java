package com.example.davidgeisinger.youanimalmeanimal;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.text.DecimalFormat;

import static java.lang.Integer.parseInt;

public class MainActivity extends AppCompatActivity {

   public int[] source_arr = new int[]{R.id.s_human, R.id.s_bear, R.id.s_cat, R.id.s_chicken,
           R.id.s_cow, R.id.s_deer, R.id.s_dog, R.id.s_duck,
           R.id.s_elephant, R.id.s_fox, R.id.s_goat, R.id.s_groundhog,
           R.id.s_guinea_pig, R.id.s_hamster, R.id.s_hippo, R.id.s_horse,
           R.id.s_kangaroo, R.id.s_lion, R.id.s_monkey, R.id.s_mouse,
           R.id.s_parakeet, R.id.s_pig, R.id.s_pigeon, R.id.s_rabbit,
           R.id.s_rat, R.id.s_sheep, R.id.s_squirrel, R.id.s_wolf};

    public int[] target_arr = new int[]{R.id.t_human, R.id.t_bear, R.id.t_cat, R.id.t_chicken,
            R.id.t_cow, R.id.t_deer, R.id.t_dog, R.id.t_duck,
            R.id.t_elephant, R.id.t_fox, R.id.t_goat, R.id.t_groundhog,
            R.id.t_guinea_pig, R.id.t_hamster, R.id.t_hippo, R.id.t_horse,
            R.id.t_kangaroo, R.id.t_lion, R.id.t_monkey, R.id.t_mouse,
            R.id.t_parakeet, R.id.t_pig, R.id.t_pigeon, R.id.t_rabbit,
            R.id.t_rat, R.id.t_sheep, R.id.t_squirrel, R.id.t_wolf};

    public double[] score = new double[] {1, 2, 3.2, 5.33, 3.64, 2.29, 3.64, 4.21, 1.14, 5.71, 5.33, 5.71,
            10, 20, 1.78, 2, 8.89, 2.29, 3.2, 20, 4.44, 3.2, 7.27, 8.89, 26.67, 5.33, 5, 4.44};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button clicker = (Button) findViewById(R.id.mikey_button);
        final TextView display = (TextView) findViewById(R.id.answer);
        final EditText input_age = (EditText) findViewById(R.id.input_age);
        final RadioGroup source = (RadioGroup) findViewById(R.id.source_radio_group);
        final RadioGroup target = (RadioGroup) findViewById(R.id.target_radio_group);

        clicker.setOnClickListener(
                new Button.OnClickListener(){
                    public void onClick(View v) {
                        solve_age(display, input_age, source, target);
                    }
                }
        );

    }

    public void solve_age(TextView display, EditText input_age, RadioGroup source, RadioGroup target) {
        if (input_age.getText().toString().equals("")) {
            err_msg_create(display);
        } else if (source.getCheckedRadioButtonId() == -1) {
            err_msg_create(display);
        } else if (target.getCheckedRadioButtonId() == -1) {
            err_msg_create(display);
        } else {

            // formatting the answer we get from our conversion to 2 decimal points
            int inp_age = parseInt(input_age.getText().toString());
            double age = get_age(inp_age, source.getCheckedRadioButtonId(), target.getCheckedRadioButtonId());
            DecimalFormat df = new DecimalFormat("#.##");
            double outputNum = Double.valueOf(df.format(age));
            String age_string = df.format(age);



            // Getting animal names
            RadioGroup rg = (RadioGroup)findViewById(R.id.source_radio_group);
            String source_string = ((RadioButton)findViewById(rg.getCheckedRadioButtonId())).getText().toString();

            RadioGroup rg1 = (RadioGroup)findViewById(R.id.target_radio_group);
            String target_string = ((RadioButton)findViewById(rg1.getCheckedRadioButtonId())).getText().toString();

            // getting input age in number form
            String input_string = input_age.getText().toString();

            // final formatting
            String final_string = input_string + " " + source_string + " years is equal to " + age_string +
                    " " + target_string + " years.";


            display.setText(final_string);
        }


    }

    public double get_age(int input_age, int source_num, int target_num) {
        int first_index = find_source_index(source_num);
        int second_index = find_target_index(target_num);
        double answer = input_age * score[second_index] / score[first_index];
        return answer;
    }

    private int find_target_index(int target_num) {
        for (int i = 0; i < target_arr.length; i++) {
            if (target_num == target_arr[i]) {
                return i;
            }
        }
        return -1;
    }


    private int find_source_index(int source_num) {
        for (int i = 0; i < source_arr.length; i++) {
            if (source_num == source_arr[i]) {
                return i;
            }
        }
        return -1;
    }

    public void err_msg_create(TextView display) {
        display.setText("Please enter an age and select a radio button from both groups");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
